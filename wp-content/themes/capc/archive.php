<?php
/**
 * The template for displaying news content.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package capc
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main container-fluid" role="main">
			<header class="page-header">
				<h1>Latest News</h1>
			</header>
			<?php
			if ( have_posts() ) :
				?>
				<div class="col-sm-9 col-md-10 capc-page-content">
				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					//check if post has featured image
					$media = get_field('media');
					if($media || has_post_thumbnail()):
					?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
						<p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php the_date(); ?>&emsp;<span class="glyphicon glyphicon-tags"></span> <?php the_category(', '); ?></p>
						<div class="row">
							<div class="col-sm-6">
								<p><?php the_excerpt();?></p>
							</div>
							<div class="col-sm-6 capc-news-media">
								<?php
								if($media) {
									if(strpos($media,"youtube")) {
										echo "<div class='capc-video'>" . $media . "</div>";
									} else {
										echo $media;
									}
								} else {
									the_post_thumbnail();
								}?>
							</div>
						</div>
					<?php
					else:
					?>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
					<p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php the_date(); ?>&emsp;<span class="glyphicon glyphicon-tags"></span> <?php the_category(', '); ?></p>
					<p><?php the_excerpt();?></p>
					<?php
					endif;
					echo '<br>';
				endwhile;
				?>
				</div>
				<div class="col-sm-3 col-md-2 capc-page-sidebar">
					<h3>Categories</h3>
					<ul class="capc-news-categories">
						<?php wp_list_categories( array ( 'title_li' => '' )); ?>
					</ul>
				</div>
				<?php
				the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
