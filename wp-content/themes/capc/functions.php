<?php
/**
 * capc functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package capc
 */

if ( ! function_exists( 'capc_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function capc_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on capc, use a find and replace
	 * to change 'capc' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'capc', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'capc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'capc_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'capc_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function capc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'capc_content_width', 640 );
}
add_action( 'after_setup_theme', 'capc_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function capc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'capc' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'capc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'capc_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function capc_scripts() {
	wp_enqueue_style( 'google-fonts', "//fonts.googleapis.com/css?family=Gabriela|Oswald|Roboto+Mono|Roboto+Slab" );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap.min.css', array(), '3.3.7' );
	wp_enqueue_style( 'capc', get_stylesheet_uri(), array(), '1.4' );
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), '3.1.0', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.7', true );
}
add_action( 'wp_enqueue_scripts', 'capc_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Register widgets
 */
function capc_widgets() {
	register_sidebar(array(
		'name'=> 'Header Widget',
		'id' => 'header',
		'before_widget' => '<div class="nav-lead">',
		'after_widget' => '</div>'
	));
}
//add_action( 'widgets_init', 'capc_widgets' );

/**
 * Template redirector
 */
function capc_template($template) {
	global $wp_query;
	if ($wp_query->is_404) {
		$wp_query->is_404 = false;
		$wp_query->is_archive = true;
	}
	header("HTTP/1.1 200 OK");
	include($template);
	exit;
}

/**
 * Use archive.php for listing latest news
 */
function capc_news() {
	global $wp;
	if ($wp->request == 'news') {
		capc_template(TEMPLATEPATH . '/archive.php');
	}
}
add_action('template_redirect', 'capc_news');

/**
 * Use /search/ for search
 */
function capc_search() {
	if ( is_search() && ! empty( $_GET['q'] ) ) {
		wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
		exit();
	}
}
add_action('template_redirect', 'capc_search');

/**
 * Excerpt that does not strip html tags
 */
function capc_trim_excerpt() {
	global $wp;
	if ($wp->request == 'news') {
		return get_the_content();
	} else {
		return wp_trim_excerpt();
	}
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'capc_trim_excerpt');

/**
 * Redirect map
 */
$redirecturls = array(
	"programs.html" => "/programs/",
	"mandated-reported-training.html" => "/programs/mandated-reported-training/",
	"baby-bag-new-parent-kit.html" => "/programs/baby-bag-new-parent-kit/",
	"nurturing-parenting-programs.html" => "/programs/nurturing-parenting-programs/",
	"nurturing-parenting-connection.html" => "/programs/nurturing-parenting-connection/",
	"childhelp-speak-up-be-safe.html" => "/programs/childhelp-speak-up-be-safe/",
	"surviving-parenthood.html" => "/programs/surviving-parenthood/",
	"annual-luncheon.html" => "/annual-luncheon/",
	"home.html" => "/",
	"news.html" => "/news/",
	"about.html" => "/about/",
	"history-timeline.html" => "/about/history/",
	"board-members.html" => "/about/board-members/",
	"proclamations---community-reports.html" => "/about/proclamations-community-reports/",
	"events.html" => "/events/",
	"contact.html" => "/contact/",
	"careers.html" => "/contact/careers/",
	"donate.html" => "/donate/",
	"partnerships.html" => "/donate/partnerships/",
	"planned-giving.html" => "/donate/planned-giving/",
	"volunteer-opportunities.html" => "/donate/volunteer-opportunities/",
	"wish-list.html" => "/donate/wish-list/"
);

remove_filter ('the_content', 'wpautop');


/**
 * Show Before and After Event content only in the month view
 */
function events_before_after_filter($content) {
	if(tribe_is_month()) {
		echo str_replace(array("tribe-events-after-html", "tribe-events-before-html"), "", $content);
	}
}
add_filter('tribe_events_after_html', 'events_before_after_filter');
add_filter('tribe_events_before_html', 'events_before_after_filter');

/**
 * Fix next month link for current month
 */
function fix_next_month_link($html) {
	if(strlen($html)==0) {
		$url  = tribe_get_next_month_link();
		$text = tribe_get_next_month_text();
		if ( ! empty( $url ) ) {
				echo '<a data-month="' . $date . '" href="' . esc_url( $url ) . '" rel="next">' . $text . ' <span>&raquo;</span></a>';
		}
	} else {
		echo $html;
	}
}
add_filter('tribe_events_the_next_month_link', 'fix_next_month_link');

/**
 * Register Header widget
 */
function header_widget_init() {
	register_sidebar(array(
    'name' => __( 'Header Area', 'capc' ),
    'id' => 'capc-header-widget',
    'description' => __( 'An optional widget area for your site header', 'capc' ),
    'before_widget' => '<div id="%1$s" class="capc-header-widget %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3>',
    'after_title' => '</h3>',
	));
}
add_action( 'widgets_init', 'header_widget_init' );
